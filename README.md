# Running Application in AKS Cluster

## Step 1: Create an Azure Kubernetes Service (AKS) Cluster

Sign in and access Azure Cloud Shell

### 1.1 Setup Cluster Variable

Create variable

```
export CLUSTER_NAME=aks-host-info
export RESOURCE_GROUP=ea-host-info
export LOCATION=eastasia
```

### 1.2 Create Resource Group

Run the az group create command to create a resource group. We will deploy any resources to this resources group

```
az group create --name=$RESOURCE_GROUP --location=$LOCATION
```

### 1.3 Provision AKS Cluster

Run the ```az aks create``` command to create an AKS cluster

```
az aks create \
    --resource-group $RESOURCE_GROUP \
    --name $CLUSTER_NAME \
    --node-count 1 \
    --enable-addons http_application_routing \
    --generate-ssh-keys \
    --node-vm-size Standard_B2s \
    --network-plugin azure
```

### 1.4 Add Node Pool to AKS Cluster

Node pool can be used to host applications and workloads. Add new node pool to newly created AKS cluster above with ```az aks nodepool``` command 

```
az aks nodepool add \
    --resource-group $RESOURCE_GROUP \
    --cluster-name $CLUSTER_NAME \
    --name userpool \
    --node-count 1 \
    --node-vm-size Standard_B2s
```

### 1.5 Setup Administrative Tool command

Setup ```kubectl``` command

```
az aks get-credentials --name $CLUSTER_NAME --resource-group $RESOURCE_GROUP
```

### 1.6 Examine Kubernetes Cluster with ```kubectl```

Retrieve Kubernetes nodes information

```
kubectl get nodes
```

---

## Step 2: Deploy application

### 2.1 Create deployment file

Create deployment ```deploy.yaml```

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: host-info
spec:
  selector: # Define the wrapping strategy
    matchLabels: # Match all pods with the defined labels
      app: host-info # Labels follow the `name: value` template
  template: # This is the template of the pod inside the deployment
    metadata:
      labels:
        app: host-info
    spec:
      nodeSelector:
        kubernetes.io/os: linux
      containers:
        - image: registry.gitlab.com/dc81/host-info
          name: host-info
          ports:
            - containerPort: 10001
              name: http
```

### 2.2 Deploy Application

Deploy host-info application in AKS cluster

```
kubectl apply -f ./deploy.yaml
```

### 2.3 Retrieve deployment information

```
kubectl get deploy host-info
```

Retrieve pods

```
kubectl get pods
```

### 2.4 Scale Application

```
kubectl scale --replicas=10 deployments host-info
```

## Step 3: Enable Network Access 

### Step 3.1

Create ```service.yaml```

```
apiVersion: v1
kind: Service
metadata:
  name: host-info
spec:
  type: ClusterIP
  selector:
    app: host-info
  ports:
    - port: 10001 # SERVICE exposed port
      name: host-info-http # SERVICE port name
      protocol: TCP # The protocol the SERVICE will listen to
      targetPort: 10001 # Port to forward to in the POD
```

### Step 3.2

Create service for our application

```
kubectl apply -f ./service.yaml
```

Retrieve information

```
kubectl get service host-info
```

## Step 4: Enable Access to Application with Ingress

### 4.1 Install Ingress Application

```
NAMESPACE=ingress-basic

helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update

helm install ingress-nginx ingress-nginx/ingress-nginx \
  --create-namespace \
  --namespace $NAMESPACE \
  --set controller.service.annotations."service\.beta\.kubernetes\.io/azure-load-balancer-health-probe-request-path"=/healthz
```

Check installation with this command

```
kubectl --namespace ingress-basic get services -o wide
```

### 4.2 Create application ingress rule

Create ```ingress.yaml```

```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: host-info
  annotations:
    nginx.ingress.kubernetes.io/ssl-redirect: "false"
    nginx.ingress.kubernetes.io/use-regex: "true"
    nginx.ingress.kubernetes.io/rewrite-target: /$2
spec:
  ingressClassName: nginx
  rules:
  - http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: host-info
            port:
              number: 10001
```

### 4.3 Enable Access with Ingress rule

```
kubectl apply -f ./ingress.yaml
```

Retrieve ingress information

```
kubectl get ingress host-info
```

Wait until IP address is shown

### Step 4.4 Access with Public IP

Access https://<IP>
